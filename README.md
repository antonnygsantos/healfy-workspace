## HEALFY

Healfy Software is a solution that looks to make the nutritionist process better.

## Pre requirements

You must have installed the following items:

1. Docker and docker-compose.
2. VueJS.
3. NPM and NodeJS.

---

## Get start

To clone project: 
git clone --recursive https://antonnygsantos@bitbucket.org/antonnygsantos/healfy-workspace.git

## To run it
docker-compose up at healfy-workspace
Api will be listen at port 3001 and client at port 3000

